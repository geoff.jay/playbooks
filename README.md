# Ansible Playbooks

Mostly used to setup new hosts for personal use.

## Setup

```sh
sudo mkdir -p /etc/ansible
echo -e "[local]\nlocalhost ansible_connection=local" | \
  sudo tee /etc/ansible/hosts
```
