# ToDo

* [ ] need to fix admin user setup in playbook
  - [ ] add tty group
  - [ ] add video group
  - [ ] add admin group as 1000 and make default
* [x] make `systemd` roles more similar to `development`
* [x] clean up variables with `vars_files`
* [x] use defaults and vars with domain role
* [x] convert domain templates to use vars
* [ ] drop work domain name from everything
