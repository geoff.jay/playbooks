# Notes

During installation needed to add these packages.

```sh
pacman -S wpa_supplicant dialog git ansible
```

Potentially relevant commands that should be done in roles.

```sh
systemctl stop dhcpcd
systemctl disable dhcpcd
systemctl enable systemd-networkd
systemctl enable systemd-resolved
systemctl start systemd-networkd
systemctl start systemd-resolved
touch 20-wired.network
touch 25-wireless.network
systemctl restart systemd-networkd
systemctl restart systemd-resolved
wpa_passphrase $WPA_SSID $WPA_PASSPHRASE > \
  /etc/wpa_supplicant/wpa_supplicant-wlp59s0.conf
systemctl enable wpa_supplicant@wlp59s0
systemctl start wpa_supplicant@wlp59s0
systemctl restart systemd-networkd
ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf
systemctl status systemd-resolved
reboot
```

## Post Installation

```sh
git clone https://github.com/kewlfft/ansible-aur.git ~/.ansible/plugins/modules/aur
git clone https://gitlab.com/geoff.jay/playbooks.git
ansible-playbook --ask-become-pass playbooks/setup.yml
```

### Xorg

```sh
# console errors
chmod u+s /usr/bin/Xorg
```
