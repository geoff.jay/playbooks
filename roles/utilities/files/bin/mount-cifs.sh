#!/bin/bash

if [ $UID -ne 0 ]; then
    echo "Must be root to mount shares"
    exit 1
fi

# dumb check
mount | egrep '^.*/mnt/smb/files.*cifs.*$' &>/dev/null
if [ $? -ne 0 ]; then
    mount -t cifs \
        -o username=gjohn,domain=coanda,iocharset=utf8,uid=1000,_netdev \
        '//fs01.coanda.local/pub-data' /mnt/smb/files
fi

mount | egrep '^.*/mnt/smb/clients.*cifs.*$' &>/dev/null
if [ $? -ne 0 ]; then
    mount -t cifs \
        -o username=gjohn,domain=coanda,iocharset=utf8,uid=1000,_netdev \
        '//fs01.coanda.local/pub-clients' /mnt/smb/clients
fi

mount | egrep '^.*/mnt/smb/home.*cifs.*$' &>/dev/null
if [ $? -ne 0 ]; then
    mount -t cifs \
        -o username=gjohn,domain=coanda,iocharset=utf8,uid=1000,_netdev \
        '//fs01.coanda.local/gjohn' /mnt/smb/home
fi
